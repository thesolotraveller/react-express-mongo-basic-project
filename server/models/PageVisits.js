const mongoose = require('mongoose');

const PageVisitsSchema = new mongoose.Schema({
  type: { type: String, required: true},
  docTypeId: { type: String, required: true },
  userId: { type: String, required: true }
});

module.exports = mongoose.model('PageVisits', PageVisitsSchema);