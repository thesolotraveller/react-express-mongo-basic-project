const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  email: { type: String, required: true, unique: true },
  name: { type: String, required: true },
  imageUrl: { type: String, required: true },
  jobRole: { type: String, required: true },
  company: {type: mongoose.Schema.Types.ObjectId, ref: 'Company'},
  salt: { type: String, required: true },
  hash: { type: String, required: true }
});

module.exports = mongoose.model('User', UserSchema);