const mongoose = require('mongoose');

const CompanySchema = new mongoose.Schema({
  name: { type: String, required: true, unique: true },
  logoUrl: { type: String, required: true },
  address: { type: String, required: true },
  pageVisits: { type: Number, required: true, default: 0 }
});

module.exports = mongoose.model('Company', CompanySchema);