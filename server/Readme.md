This is the client side for a small brushing up project on

- `react`
- `express`
- `mongodb`

`Below is the data present that can be used to test the application.`

`Company 1 - Actigage`
    Employees
        Mohit Gupta
            email: '09mohit1994@gmail.com'
            password: 'password'
        John Doe
            email: 'john@doe.com'
            password: 'password'
        Shinchan Nuhara
            email: 'shinchan@hungama.com'
            password: 'password'
        Nobita
            email: 'nobita@hungama.com'
            password: 'password'
        Jian
            email: 'jian@hungama.com'
            password: 'password'

`Company 2 - Google`
    Employees
        Danearys
            email: 'danearys@got.com'
            password: 'password'
        John
            email: 'john@got.com'
            password: 'password'
        Jaimi
            email: 'jaimi@got.com'
            password: 'password'
        Tyrion
            email: 'tyrion@got.com'
            password: 'password'
        Rob
            email: 'rob@got.com'
            password: 'password'

`We can create more companies and employees as well`      