/**
 * isAuthenticated - This is a policy for validating access token for protected api endpoints
 */

const Promise = require('bluebird');
const jwt = Promise.promisifyAll(require('jsonwebtoken'));

module.exports = function (req, res, next) {

  let secretKey = process.env.SECRET;
  const bearerHeader = req.headers['authorization'];

  if (!bearerHeader) {
    return res.status(403).json({
      error: {
        code: 'InvalidTokenType',
        message: 'Access token not supplied properly'
      }
    })
  }

  const bearer = bearerHeader.split(' ');
  const bearerToken = bearer[1];
    jwt.verify(bearerToken, secretKey, (err, authData) => {
      if(err) {
        console.log(err);

        if (err.name === 'TokenExpiredError') {
          return res.status(403).json({
            error: {
              code: 'InvalidToken',
              message: 'Invalid Access Token. Maybe the token is expired.'
            }
          })
        }

        return res.status(500).json({
          error: {
            message: "Sorry, there was an error in serving your the requested resource",
            internalMessage: err.details
          }
        });
      } 
      else {
        req.authData = authData;
        return next();
      }
    });
};
