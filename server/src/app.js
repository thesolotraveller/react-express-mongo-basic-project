// requiring module to read credentials from .env
require('dotenv').config();

// importing npm dependecies
const express    = require('express')
const bodyParser = require('body-parser')
const cors       = require('cors');
const mongoose   = require('mongoose');

// mongoDb connection string
const mongo_uri = `mongodb://${process.env.DB_HOST}/${process.env.DB_NAME}`;

// connecting to mongoDb
mongoose.connect(mongo_uri, { useNewUrlParser: true }, function(err) {
  if (err) throw err;
  console.log(`\n\nSuccessfully connected to ${mongo_uri}\n\n`);
});

// importing custom modules
const handlers = require ('./handler');

// importing middleware policies
const isAuthenticated = require('../policies/isAuthenticated');

const expressApp = () => {

	const app = express();
	
	app.use(bodyParser.urlencoded({ extended: false }));
	app.use(bodyParser.json());
	app.use(cors());

	// user routes
	app.post ('/user', handlers.registerUser);
	app.get  ('/user', isAuthenticated, handlers.getUser);
	app.post ('/login', handlers.userLogin);

	// company routes
	app.post ('/company', handlers.registerCompany);
	app.get  ('/company/:id', isAuthenticated, handlers.getCompany);
	app.post ('/company/:id', isAuthenticated, handlers.updateCompany);

	// page visits
	app.post ('/incrementpagevisits', isAuthenticated, handlers.incrementActiveUniquePageUsers)
	app.post ('/decrementpagevisits', isAuthenticated, handlers.decrementActiveUniquePageUsers)

	return app;

}

module.exports = {
  expressApp: expressApp
}