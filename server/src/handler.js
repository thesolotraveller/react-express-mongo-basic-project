require('dotenv').config();
const mongoose = require('mongoose');
const Promise = require('bluebird');
const bcrypt = Promise.promisifyAll(require('bcrypt'));
const jwt = require('jsonwebtoken');

// services
const ValidatorService = require('../services/ValidatorService');

// models
const Company = require('../models/Company');
const User = require('../models/User');
const PageVisits = require('../models/PageVisits');

// controller for POST /company
const registerCompany = (req, res) => {

  let schema = ValidatorService.Schema.object().keys({
    name: ValidatorService.Schema.string().required().label('Company Name'),
    logoUrl: ValidatorService.Schema.string().uri().required().label('Company Logo Url'),
    address: ValidatorService.Schema.string().required().label('Company Address')
  });

  ValidatorService.validate(req.body, schema)
  .then(validatedRequestBody => {
    const company = new Company(validatedRequestBody);
    return company.save(validatedRequestBody);
  })
  .then(newCompany => {
    return res.status(201).json(newCompany);
  })
  .catch(function (err) {
    console.log(err);

    if (err.name === 'ValidationError') {
      return res.status(400).json({
        message: 'Validation error',
        errors: err.details
      });
    }

    return res.status(500).json({
      error: {
        message: "Sorry, there was an error in serving your the requested resource",
        internalMessage: err.details,
        code: err.code,
        status: 500
      }
    });
  });


}

// controller for POST /user
const registerUser = (req, res) => {

  let body;

  let schema = ValidatorService.Schema.object().keys({
    name: ValidatorService.Schema.string().required().label('User Name'),
    email: ValidatorService.Schema.string().email({ minDomainSegments: 2 }).required().label('User Email'),
    imageUrl: ValidatorService.Schema.string().uri().required().label('Company Logo Url'),
    jobRole: ValidatorService.Schema.string().required().label('User Designation'),
    company: ValidatorService.Schema.string().required().label('User Company'),
    password: ValidatorService.Schema.string().required().label('User Password')
  });

  ValidatorService.validate(req.body, schema)
  .then(validatedRequestBody => {
    body = validatedRequestBody;

    return User.findOne({ email: body.email });
  })
  .then(foundUser => {
    if (foundUser) {
      throw new Error ('UserAlreadyRegistered');
    }

    return bcrypt.genSalt(10);
  })
  .then(salt => {
    body.salt = salt;

    return bcrypt.hash(body.password, body.salt);
  })
  .then(hash => {
    body.hash = hash;
    delete body.password;
    
    const user = new User(body);
    return user.save(body);
  })
  .then(newUser => res.status(201).json(newUser))
  .catch(err => {
    console.log(err);

    if (err.name === 'ValidationError') {
      return res.status(400).json({
        message: 'Validation error',
        errors: err.details
      });
    } else if (err.message === 'UserAlreadyRegistered') {
      return res.status(400).json({
        message: err.message
      });
    }

    return res.status(500).json({
      error: {
        message: "Sorry, there was an error in serving your the requested resource",
        code: err.code
      }
    });

  });

}

// controller for POST /login
const userLogin = (req, res) => {

  let body, userBody;

  let schema = ValidatorService.Schema.object().keys({
    email: ValidatorService.Schema.string().email({ minDomainSegments: 2 }).required().label('User Email'),
    password: ValidatorService.Schema.string().required().label('User Password')
  });

  ValidatorService.validate(req.body, schema)
  .then(validatedRequestBody => {
    body = validatedRequestBody;

    return User.findOne({ email: body.email }).exec();
  })
  .then(foundUser => {
    if (!foundUser) {
      throw new Error ('UserNotFound');
    }

    userBody = foundUser;

    return bcrypt.hash(body.password, foundUser.salt);
  })
  .then(hash => {
    if (hash !== userBody.hash) {
      throw new Error ('InvalidPassword');
    }

    const payload = body.email;
    const token = jwt.sign( { email: payload, id: userBody._id }, process.env.SECRET, { expiresIn: '1h' });

    return res.status(200).json({
      token: token,
      user: {
        name: userBody.name,
        jobRole: userBody.jobRole,
        company: userBody.company,
        imageUrl: userBody.imageUrl,
        id: userBody._id
      }
    })
  })
  .catch(err => {
    console.log(err);

    if (err.name === 'ValidationError') {
      return res.status(400).json({
        message: 'Validation error',
        errors: err.details
      });
    } else if (err.message === 'UserNotFound' || err.message === 'InvalidPassword') {
      return res.status(400).json({
        message: err.message
      });
    }

    return res.status(500).json({
      error: {
        message: "Sorry, there was an error in serving your the requested resource",
        code: err.code
      }
    });

  });

}

// controller for POST /company/:id
const getCompany = (req, res) => {
  const companyId = req.params.id;

  return Company.findOne({ _id: companyId })
    .then(company => {
      if (!company) throw new Error ('CompanyNotFound');

      return res.status(200).json(company);
    })
    .catch(err => {
      console.log(err);

      if (err.message === 'CompanyNotFound') {
        return res.status(400).json({
          message: err.message
        });
      }
  
      return res.status(500).json({
        error: {
          message: "Sorry, there was an error in serving your the requested resource",
          code: err.code
        }
      });
    })
}

// controller for POST /company/:id
const updateCompany = (req, res) => {

  let body, companyId = req.params.id;

  let schema = ValidatorService.Schema.object().keys({
    name: ValidatorService.Schema.string().label('Company Name'),
    logoUrl: ValidatorService.Schema.string().uri().label('Company Logo Url'),
    address: ValidatorService.Schema.string().label('Company Address'),
    pageVisits: ValidatorService.Schema.number().label('Page Visits for this company page'),
  });

  ValidatorService.validate(req.body, schema)
  .then(validatedRequestBody => {
    body = validatedRequestBody;

    return Company.findOne({ _id: companyId })
  })
  .then(company => {
    if (!company) throw new Error ('CompanyNotFound');

    return Company.update({ _id: companyId }, body);
  })
  .then(updatedCompany => res.status(200).json(updatedCompany))
  .catch(err => {
    console.log(err);

    if (err.name === 'ValidationError') {
      return res.status(400).json({
        message: 'Validation error',
        errors: err.details
      });
    } else if (err.message === 'CompanyNotFound') {
      return res.status(400).json({
        message: err.message
      });
    }

    return res.status(500).json({
      error: {
        message: "Sorry, there was an error in serving your the requested resource",
        internalMessage: err.details,
        code: err.code,
        status: 500
      }
    });
  });


}

// controller from GET /user
const getUser = (req, res) => {
  const userId = req.authData.id;

  User.findOne({ _id: userId })
    .populate('company', 'name')
    .then(user => {
      user.hash = null;
      user.salt = null;

      return res.status(200).json(user);
    })
    .catch(err => {
      console.log(err);
  
      return res.status(500).json({
        error: {
          message: "Sorry, there was an error in serving your the requested resource",
          code: err.code
        }
      });
    })
}

// controller for POST /pagevisits
const incrementActiveUniquePageUsers = (req, res) => {

  let body, count;

  let schema = ValidatorService.Schema.object().keys({
    type: ValidatorService.Schema.string().required().valid('company').label('Document Type'),
    docTypeId: ValidatorService.Schema.string().required().label('Document Id')
  });

  ValidatorService.validate(req.body, schema)
    .then(validatedRequestBody => {
      body = validatedRequestBody;

      return PageVisits.find(body);
    })
    .then(result => {

      let userId = req.authData.id;
      let isCurrentViewCounted = false;

      body.userId = userId;
      count = result.length;

      for (let i=0; i<result.length; i++) {
        if (result[i].userId === userId) {
          isCurrentViewCounted = true;
          break;
        }
      }

      if (!isCurrentViewCounted) {
        count++;
        const pageVisit = new PageVisits(body); 
        return pageVisit.save(body);
      }

      return null;
    })
    .then(() => {
      return res.status(200).json({uniqueActiveUsers: count});
    })
    .catch(err => {
      if (err.name === 'ValidationError') {
        return res.status(400).json({
          message: 'Validation error',
          errors: err.details
        });
      }
  
      return res.status(500).json({
        error: {
          message: "Sorry, there was an error in serving your the requested resource",
          code: err.code
        }
      });
    })
}

// controller for POST /pagevisits
const decrementActiveUniquePageUsers = (req, res) => {

  let schema = ValidatorService.Schema.object().keys({
    type: ValidatorService.Schema.string().required().valid('company').label('Document Type'),
    docTypeId: ValidatorService.Schema.string().required().label('Document Id')
  });

  ValidatorService.validate(req.body, schema)
    .then(validatedRequestBody => {
      validatedRequestBody.userId = req.authData.id;

      return PageVisits.deleteOne(validatedRequestBody);
    })
    .then(() => {
      return res.status(200).json({});
    })
    .catch(err => {
      console.log(err);

      if (err.name === 'ValidationError') {
        return res.status(400).json({
          message: 'Validation error',
          errors: err.details
        });
      }
  
      return res.status(500).json({
        error: {
          message: "Sorry, there was an error in serving your the requested resource",
          code: err.code
        }
      });
    })
}

module.exports = {
  registerCompany: registerCompany,
  registerUser: registerUser,
  userLogin: userLogin,
  getCompany: getCompany,
  updateCompany: updateCompany,
  getUser: getUser,
  incrementActiveUniquePageUsers: incrementActiveUniquePageUsers,
  decrementActiveUniquePageUsers: decrementActiveUniquePageUsers
}