import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Login from './components/Login/Login';
import UserProfile from './components/UserProfile/UserProfile';
import CompanyProfile from './components/CompanyProfile/CompanyProfile';
import NoMatch from './components/NoMatch/NoMatch';

import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/login" component={Login} />
          <Route path="/profile" component={UserProfile} />
          <Route path="/user/company/:companyId" component={CompanyProfile} />
          <Route component={NoMatch} />
        </Switch>
      </header>
    </div>
  );
}

export default App;