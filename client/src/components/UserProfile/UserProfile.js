import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

import { server } from '../../config';

export default class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      redirect: false,
      userData: {},
      companyId: ''
    };
  }

  componentDidMount() {
    if (!localStorage.getItem('token')) {
      this.setState({redirect: true});
    } else {
      axios.get(`${server}/user`, {
          headers: {
            'Authorization': 'Bearer ' + localStorage.getItem('token')
          }
        })
        .then(res => {
          if (!res.status || res.status !== 200) {
            throw new Error ('Error Fetching User Data');
          }
          this.setState({ userData: res.data })
        })
        .catch(err => {
          localStorage.removeItem('token');
          this.setState({redirect: true});
        })
    }
  }

  visitCompanyProfile (e) {
    let companyId = this.state.userData.company._id;
    this.props.history.push('/user/company/' + companyId);
  }

  userLogout (e) {
    localStorage.removeItem('token');
    this.setState({redirect: true});
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="login" />
    }
    if (this.state.userData !== {}) {
      const data = this.state.userData;
      return (
        <div>
          <img src={data.imageUrl} alt="Profile" />
          <h1>Name: {data.name}</h1>
          <h3>Designation: {data.jobRole}</h3>
          <button onClick={this.visitCompanyProfile.bind(this)}>
            <h3>Company: {data.company && data.company.name ? data.company.name : ''}</h3>
          </button><br/>
          <button onClick={this.userLogout.bind(this)}>
            <h3>Logout</h3>
          </button>
        </div>
      );
    }
  }
}