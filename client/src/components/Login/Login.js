import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

import { server } from '../../config';

export default class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email : '',
      password: '',
      redirect: false
    };
  }

  componentDidMount() {
    if (localStorage.getItem('token')) {
      this.setState({redirect: true});
    }
  }

  handleInputChange = (event) => {
    const { value, name } = event.target;
    this.setState({
      [name]: value
    });
  }

  onSubmit = (event) => {
    event.preventDefault();
    const { email, password } = this.state;
    axios.post(`${server}/login`, { email, password }, {
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
      if (!res.status || res.status !== 200) {
        throw new Error ('User Login Failed');
      }

      const authData = res.data;

      // storing session data to local storage
      localStorage.setItem('token', authData.token);

      // redirecting to UserProfile page
      this.setState({redirect: true});
    })
    .catch(err => {
      alert(err);
    });
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/profile" />;
    } else {
      return (
        <form onSubmit={this.onSubmit}>
          <h1>Login Below!</h1>
          <input
            type="email"
            name="email"
            placeholder="Enter email"
            value={this.state.email}
            onChange={this.handleInputChange}
            required
          />
          <input
            type="password"
            name="password"
            placeholder="Enter password"
            value={this.state.password}
            onChange={this.handleInputChange}
            required
          />
          <input type="submit" value="Submit"/>
        </form>
      );
    }    
  }
}