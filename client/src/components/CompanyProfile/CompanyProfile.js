import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import PageVisibility from 'react-page-visibility';

import { server } from '../../config';

export default class Login extends Component {

  constructor(props) {
    super(props)
    this.state = {
      redirect: false,
      companyData: {},
      pageVisits: '',
      uniqueActiveViewers: 0,
      companyId: this.props.match.params.companyId
    };
  }

  componentDidMount() {
    if (!localStorage.getItem('token')) {
      this.setState({redirect: true});
    } else {
      axios.get(`${server}/company/${this.state.companyId}`, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } })
        .then(res => {
          if (!res.status || res.status !== 200) {
            throw new Error ('Error Fetching Company Data');
          }
          this.setState({ companyData: res.data, pageVisits: res.data.pageVisits })
          
          return axios.post(`${server}/company/${this.state.companyId}`, {
            pageVisits: this.state.companyData.pageVisits + 1
          },{ headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } })
        })
        .then(res => {
          if (res.state === 200) {
            this.setState({ pageVisits: this.state.pageVisits + 1});
          }
          
          return axios.post(`${server}/incrementpagevisits`, {
            type: 'company',
            docTypeId: this.state.companyId
          }, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } })
        })
        .then(res => {
          if (res.status === 200) {
            this.setState({uniqueActiveViewers: res.data.uniqueActiveUsers});
          }
        })
        .catch(err => {
          localStorage.removeItem('token');
          this.setState({redirect: true});
        })
    }
  }

  userLogout (e) {
    const body = { type: 'company', docTypeId: this.state.companyId };
    axios.post(`${server}/decrementpagevisits`, body, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } })
      .then(res => {
        if (res.status === 200) {
          localStorage.removeItem('token');
          this.setState({redirect: true});
        }
      })
  }

  handleVisibilityChange = isVisible => {
    const body = { type: 'company', docTypeId: this.state.companyId };

    if (isVisible) {
      axios.post(`${server}/incrementpagevisits`, body, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } })
        .then(res => {
          if (res.status === 200) {
            this.setState({uniqueActiveViewers: res.data.uniqueActiveUsers});
          }
        })
    } else {
      axios.post(`${server}/decrementpagevisits`, body, { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } })
        .then(res => {
          if (res.status === 200) {
            console.log(res.data);
          }
        })
    }
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/login" />
    }
    if (this.state.companyData !== {}) {
      const data = this.state.companyData;
      return (
        <div>
          <PageVisibility onChange={this.handleVisibilityChange}>
            <img src={data.logoUrl} alt="Profile" />
          </PageVisibility>
          <h1>Name: {data.name}</h1>
          <h3>Location: {data.address}</h3>
          <h3>Page Visits: {data.pageVisits}</h3>
          <h3>Active Users: {this.state.uniqueActiveViewers}</h3>
          <button onClick={this.userLogout.bind(this)}>
            <h3>Logout</h3>
          </button>
        </div>
      );
    }
  }

}