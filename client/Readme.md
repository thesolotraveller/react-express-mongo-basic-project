This is the client side for a small brushing up project on

- `react`
- `express`
- `mongodb`

and has been bootstraped with `create-react-app` utility by `Facebook`

Client Side is Hosted @ `http://previous-lunch.surge.sh`

Client is making network request to api server hosted on AWS and the link to
the API server is `https://mohit.work/apiPratilipi`

Please checkout `~/server/Readme.md` for info on datasets are present to test
this small web application